package dkeep.cli;

import java.util.Scanner;
import dkeep.logic.Game;

public class Main {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
				
			Scanner scan = new Scanner(System.in);
			char whereToGo;
			boolean isGameOver = false;
			int nDragons = askNumberOfDragons(scan);
			Game game = new Game(nDragons);
			
			do {
				printDungeon(game.getDungeon());
				System.out.print("Enter your next move: ");
				whereToGo = scan.next().charAt(0);
				isGameOver = game.input(whereToGo);
				if (!isGameOver)
					System.out.println(game.getMessage());
			} while (!(isGameOver) && (whereToGo != 'x'));
				
			printDungeon(game.getDungeon());
			System.out.println(game.getMessage());
			
			if (whereToGo == 'x')
				System.out.println("This time, the hero's quest will be left untold...");
			
			scan.close();
	}
	
	private static int askNumberOfDragons(Scanner scan) {
		do {
			System.out.print("How many Dragons are in the Dungeon? (1-4)? ");
			String n = scan.next();
			try {
				int nDragons = Integer.parseInt(n);
				if (nDragons < 1 || nDragons > 4)
					throw new NumberFormatException();
				return nDragons;
			} catch (NumberFormatException e) {
				System.out.println("Please Enter a valid number!");
			}
		} while (true);
	}
	
	public static void printDungeon(char[][] dungeon)
	{
		for (int line = 0; line < dungeon.length; line++)
		{
			for (int column = 0; column < dungeon[line].length; column++)
			{
				System.out.print(dungeon[line][column]);
			}
			System.out.println();
		}
		
		System.out.println();
	}
}
