package dkeep.logic;

import java.util.ArrayList;
import java.util.Random;

public class Game {
		
	static String message;
	Dungeon dungeon;
	Hero hero;
	Element sword;
	Element exit;
	ArrayList<Dragon> dragons;
	Random rand = new Random();
	
	public Game(int nDragons) {
		message = "";
		dungeon = new Dungeon();
		hero = new Hero(1,1, dungeon);
		sword = new Element(1,8,'S', dungeon);
		//exit = new Element(9,4,'E',dungeon);   //Fixed placing of Exit
		exit = new Element('E', dungeon);   //Random placing of Exit
		enterTheDragons(nDragons);
	}

	public String getMessage() {
		return message;
	}
	
	public char[][] getDungeon () {
		return dungeon.getDungeon();
	}
	
	public boolean input(char whereToGo) {
		
		message = "";
		
		switch (whereToGo) {
		case 'w':
			return update(Dungeon.Direction.NORTH);
		case 'a':
			return update(Dungeon.Direction.WEST);
		case 's':
			return update(Dungeon.Direction.SOUTH);
		case 'd':
			return update(Dungeon.Direction.EAST);
		}
		
		return false;
	}
	
	public boolean update(Dungeon.Direction dir) {
		
		boolean heroKilled = false;
		boolean victorious = tryToExit(dir);
		
		if (!victorious) {
			hero.move(dir);
			tryToPickSword();
			heroKilled = dragonsAttack();
			if (!heroKilled) heroKilled = dragonsMove();
		}
		
		return victorious || heroKilled;
	}
	
	public boolean tryToExit(Dungeon.Direction dir) {
		
		boolean heroTriesToExit = false;
		
		if (hero.adjacentTo(exit)) {
			
			switch (dir) {
			case NORTH:
				if (hero.x == exit.x && hero.y - 1 == exit.y) heroTriesToExit = true;
				break;
			case WEST:
				if (hero.x - 1 == exit.x && hero.y == exit.y) heroTriesToExit = true;
				break;
			case SOUTH:
				if (hero.x == exit.x && hero.y + 1 == exit.y) heroTriesToExit = true;
				break;
			case EAST:
				if (hero.x + 1 == exit.x && hero.y == exit.y) heroTriesToExit = true;
				break;
				default:
					heroTriesToExit = false;
			}
			
			if(heroTriesToExit) {
				if (hero.hasFoundKey()) {
					message  = "By bravely slaying every Dragon in the dungeon, "
							+ "the Hero emerges victorious yet again!";
					dungeon.placeAtCell(hero.x, hero.y, ' ');
					dungeon.placeAtCell(exit.x, exit.y, hero.symbol);
					return true;
				} else {
					message = "The exit door is locked. Slay every Dragon in the dungeon to open it!";
					return false;
				}
			}
		}
		
		return false;
	}
	
	public void tryToPickSword() {
		if (hero.hasfoundSword()) return;
		else {
			if (hero.onTopOf(sword)) hero.setFoundSword(true);
		}
	}
	
	private void enterTheDragons(int nDragons) {
		dragons = new ArrayList<Dragon>(nDragons);
		dragons.add(new Dragon(1, 5, dungeon));
		
		for (int i = 0; i < nDragons - 1; i++) {
			dragons.add(new Dragon(8, 2 + 3*i, dungeon));
		}
	}		
	
	public boolean dragonsAttack() {
		
		for (Dragon dragon : dragons) {
			if (dragon.isAlive && hero.adjacentTo(dragon)) {
				if (hero.hasfoundSword()) {
					dragon.killed();
					dungeon.placeAtCell(dragon.x, dragon.y, ' ');
					hero.setFoundKey(areAllDragonsSlain());
				} else {
					message = "And thus the hero has failed in his quest."
						+ " Alas, he has been slain by one of the deadly Dragons ... Game Over";
					return true;	
				}
			}
		}
	
		return false;
	}
	
	public boolean dragonsMove() {
		for (Dragon dragon : dragons) {
			if (!dragon.isAlive)  continue;
			while (!dragon.move(Dungeon.Direction.values()[rand.nextInt(Dungeon.Direction.values().length)]));
		}
		
		return dragonsAttack();
	}
	
	public boolean areAllDragonsSlain() {
		boolean allDragonsSlain = false;
		
		for (Dragon dragon : dragons) {
			allDragonsSlain |= dragon.isAlive;
		}
		
		return !allDragonsSlain;
	}
}
