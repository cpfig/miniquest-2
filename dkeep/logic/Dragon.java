package dkeep.logic;

public class Dragon extends Element {
	
	boolean isAlive;	
	boolean isOnTopOfSword;
	
	public Dragon(int x, int y, Dungeon dungeon) {
		super(x, y, 'D', dungeon);
		this.isAlive = true;
		this.isOnTopOfSword = false;
	}
	
	public void killed() {
		this.isAlive = false;
	}
	
	public boolean move(Dungeon.Direction dir) {
		int deltaX = 0;
		int deltaY = 0;
		
		switch (dir) {

		case NORTH: deltaX = 0; deltaY = -1;
		break;
		case NORTHWEST: deltaX = -1; deltaY = -1;
		break;
		case WEST: deltaX = -1; deltaY = 0;
		break;
		case SOUTHWEST: deltaX = -1; deltaY = 1; 
		break;
		case SOUTH: deltaX = 0; deltaY = 1;
		break;
		case SOUTHEAST: deltaX = 1; deltaY = 1;
		break;
		case EAST: deltaX = 1; deltaY = 0;
		break;
		case NORTHEAST: deltaX = 1; deltaY = -1;
		break;
		}
		
		if (!dungeon.canMoveInto(x + deltaX, y + deltaY)) return false;
		
		//More convenient to treat dragon over sword in this method instead of in the Game class
		//because here we have info on dragon's current and next positions
		
		boolean dragonAwayFromSword = false;
		
		if (isOnTopOfSword) {
			this.symbol = 'D';
			dragonAwayFromSword = true;
			isOnTopOfSword = false;
		} else {
			if (dungeon.checkCharAtCell(x + deltaX, y + deltaY, 'S')) {
				this.symbol = 'F';
				isOnTopOfSword = true;
			}
		}
		
		dungeon.moveElement(symbol, x, deltaX, y, deltaY);
		if (dragonAwayFromSword) dungeon.moveElement('S', x, 0, y, 0);
		x += deltaX;
		y += deltaY;
		
		return true;
	}
}
