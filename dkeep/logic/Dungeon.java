package dkeep.logic;

public class Dungeon {
	
	public enum Direction {NORTH, WEST, SOUTH, EAST, NORTHWEST, SOUTHWEST, SOUTHEAST, NORTHEAST};
	
	public enum Walls {UPPER, LEFT, BOTTOM, RIGHT};
	
	char[][] dungeon =
		{
			{'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X'},
			{'X', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'X'},
			{'X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X'},
			{'X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X'},
			{'X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X'},
			{'X', ' ', ' ', ' ', ' ', ' ', ' ', 'X', ' ', 'X'},
			{'X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X'},
			{'X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X'},
			{'X', ' ', 'X', 'X', ' ', ' ', ' ', ' ', ' ', 'X'},
			{'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X'}
		};
	
	int lengthX = dungeon[0].length;
	int lengthY = dungeon.length;
	
	public char[][] getDungeon() {
		return dungeon;
	}
	
	public void placeAtCell (int x, int y, char c) {
		dungeon[y][x] = c;
	}
	
	public boolean checkCharAtCell (int x, int y, char c) {
		return dungeon[y][x] == c;
	}

	
	public boolean placeAtWall (int x, int y, char c, Walls val) {
		
		switch (val) {
		case UPPER:
			if (dungeon[y+1][x] != ' ') return false;
			break;

		case LEFT:
			if (dungeon[y][x+1] != ' ') return false;
			break;

		case BOTTOM:
			if (dungeon[lengthY-2][x] != ' ') return false;
			break;
	
		case RIGHT:
			if (dungeon[y][lengthX-2] != ' ') return false;
			break;
	
		default:
			return false;
		}
			
			dungeon[y][x] = c;
			return true;
	}
	
	public boolean canMoveInto (int x, int y) {
		//including the Dragons solves Dragon collisions, and there is other code
		//that never allows the hero to collide with the dragon
		return (dungeon[y][x] == ' ' || dungeon[y][x] == 'S' || dungeon[y][x] == 'D');
	}
	
	public void moveElement (char symbol, int x, int deltaX, int y, int deltaY) {
		dungeon[y][x] = ' ';
		dungeon[y+deltaY][x+deltaX] = symbol;
	}

}
